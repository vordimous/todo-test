package todo

import (
	"encoding/json"
	"hash/fnv"

	"a.j/testTodo/logger"
)

/// Todo ...
type Todo struct {
	Hash      uint32
	Message   string
	Completed bool
}

// Client ...
type Client struct {
	todos     []*Todo
	hashIndex map[uint32]*Todo
}

// NewClient ...
func NewClient() *Client {
	var c Client
	c.hashIndex = make(map[uint32]*Todo)
	return &c
}

// Show ...
func (c *Client) Show() {
	t, _ := json.MarshalIndent(c.todos, "", "  ")
	logger.Info(string(t))
	// h, _ := json.MarshalIndent(c.hashIndex, "", "  ")
	// logger.Info(string(h))
}

// Add ...
func (c *Client) Add(text string) []*Todo {
	newTodo := createTodo(text)
	if todo, found := c.hashIndex[newTodo.Hash]; found {
		todo.Completed = newTodo.Completed
	} else {
		c.todos = append(c.todos, newTodo)
		c.hashIndex[newTodo.Hash] = newTodo
	}
	return c.todos
}

// Remove ...
func (c *Client) Remove(text string) <-chan bool {
	r := make(chan bool)
	h := hash(text)
	if todo, found := c.hashIndex[h]; found {
		delete(c.hashIndex, h)
		go func() {
			// simulate very long running task
			// time.Sleep(time.Millisecond * time.Duration(rand.Int63n(2000)))
			findIndex := c.findTodoIndex(todo)
			if i := <-findIndex; i >= 0 {
				c.todos = append(c.todos[:i], c.todos[i+1:]...)
				r <- found
			}
		}()
	} else {
		r <- found
	}
	return r
}

// CheckTodo ...
func (c *Client) CheckTodo(text string) {
	t := c.GetTodo(text)
	t.Completed = true
}

// UnCheckTodo ...
func (c *Client) UnCheckTodo(text string) {
	t := c.GetTodo(text)
	t.Completed = false
}

// GetTodo ...
func (c *Client) GetTodo(s string) *Todo {
	return c.hashIndex[hash(s)]
}

// findTodoIndex ...
func (c *Client) findTodoIndex(t *Todo) <-chan int {
	r := make(chan int)
	go func() {
		found := false
		for k, v := range c.todos {
			if t == v {
				found = true
				r <- k
				break
			}
		}
		if !found {
			r <- -1
		}
	}()
	return r
}

func createTodo(text string) *Todo {
	return &Todo{
		Hash:      hash(text),
		Message:   text,
		Completed: false,
	}
}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
