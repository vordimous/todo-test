package todo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNewClient...
func TestNewClient(t *testing.T) {
	c := NewClient()
	assert.Equal(t, len(c.todos), 0)
	assert.Equal(t, len(c.hashIndex), 0)
}

// TestAdd ...
func TestAdd(t *testing.T) {
	c := NewClient()
	m := "test"
	c.Add(m)
	assert.Equal(t, c.GetTodo(m).Message, m)
}

// TestRemove ...
func TestRemove(t *testing.T) {
	c := NewClient()
	m := "test"
	c.Add(m)
	assert.Equal(t, len(c.todos), 1)
	c.Remove(m)
	assert.Equal(t, len(c.todos), 0)
}

// TestCheckTodo ...
func TestCheckTodo(t *testing.T) {
	c := NewClient()
	m := "test"
	c.Add(m)
	c.CheckTodo(m)
	assert.Equal(t, c.GetTodo(m).Completed, true)

}

// TestUnCheckTodo ...
func TestUnCheckTodo(t *testing.T) {
	c := NewClient()
	m := "test"
	c.Add(m)
	c.CheckTodo(m)
	assert.Equal(t, c.GetTodo(m).Completed, true)
	c.UnCheckTodo(m)
	assert.Equal(t, c.GetTodo(m).Completed, false)
	c.CheckTodo(m)
	assert.Equal(t, c.GetTodo(m).Completed, true)
	c.Add(m)
	assert.Equal(t, c.GetTodo(m).Completed, false)
}

// TestGetTodo ...
func TestGetTodo(t *testing.T) {
	c := NewClient()
	m := "test"
	c.Add(m)
	assert.NotNil(t, c.GetTodo(m), true)
}
