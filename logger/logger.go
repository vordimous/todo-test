package logger

import (
	"fmt"
	"time"
)

// Info ...
func Info(text string) {
	fmt.Println(fmtLog(text, info))
}

// Warn ...
func Warn(text string) {
	fmt.Println(fmtLog(text, warn))
}

// Debug ...
func Debug(text string) {
	fmt.Println(fmtLog(text, debug))
}

// Error ...
func Error(text string) {
	fmt.Println(fmtLog(text, err))
}

func fmtLog(text string, level logLevel) string {
	return fmt.Sprintf("%s: %s >> %s", time.Now().Format(time.UnixDate), level.String(), text)
}

type logLevel int

const (
	info logLevel = iota
	warn
	debug
	err
)

func (l logLevel) String() string {
	return [...]string{"Info", "Warn", "Debug", "Error"}[l]
}
