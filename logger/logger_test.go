package logger

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFmtLog(t *testing.T) {
	msg := fmtLog("test", warn)
	assert.Contains(t, msg, "test")
	assert.Contains(t, msg, "Warn")
}
