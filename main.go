package main

import (
	log "a.j/testTodo/logger"
	t "a.j/testTodo/todo"
)

func main() {
	log.Info("Loaded Test Todo")
	tc := t.NewClient()

	task := "Work on task"
	_ = tc.Add(task)
	_ = tc.Add(task + " another")
	rem := tc.Remove(task + " another")
	tc.Show()
	tc.CheckTodo(task)
	tc.Show()
	tc.UnCheckTodo(task)
	tc.Show()
	_ = tc.Add(task)
	tc.Show()
	if success := <-rem; success {
		log.Info("Finished")
		tc.Show()
	}
}
